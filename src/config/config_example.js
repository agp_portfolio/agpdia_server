module.exports = {
    port: process.env.PORT || 3001,
    dbUrl: process.env.MONGODB || "mongodb://myDatabaseUrl/databaseName",
    SECRET_TOKEN: 'MySuperSecretKey'
}