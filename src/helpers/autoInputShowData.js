const moment = require("moment");
const Record = require("../models/accounts/Record");
const Concept = require("../models/accounts/Concept");
// const Category = require("../../models/accounts/Category");
const Account = require("../models/accounts/Account");
const Vehicle = require("../models/Vehicle");
const House = require("../models/House");

const user_id = "60ca41ad5d6c462cb0e9233e";
var prevMilometer = 0;
var sumStock = 0;
var fillPrevious = false;
var initialStock = 4000;

function puntualInputs() {
  console.log("Insertamos registros puntuales");
  IncPunStock();
  ExpPunHouseGen();
  ExpPunCarGen();
}

// Ingreso acciones inicial (No periodico)
//     - Categoría: Acciones
//     - Concepto: Fondos indexados
async function IncPunStock() {
  console.log("Añadiendo ingreso puntual de acciones");
  sumStock = initialStock;
  formInfo = {
    note: "Compra de acciones",
    amount: `-${sumStock}`,
    account_id: "60ca58155d6c462cb0e92364",
    account_to_id: "60ca58495d6c462cb0e92365",
    date: "06/18/2017",
    type: "transfer",
    transfer_type: "stock",
  };
  await addRecord(formInfo);
}

// Gasto rehabilitacion:
//     - Categoría: Vivienda
//     - Concepto: Rehabilitacion
//     - Nota: Reforma

async function ExpPunHouseGen() {
  console.log("Añadiendo ingreso puntual de vivienda");
  formInfo = {
    note: "Reforma inicial vivienda",
    concept_id: "60ca46025d6c462cb0e92351",
    amount: "-4580",
    account_id: "60ca58155d6c462cb0e92364",
    house_id: "60ca439e5d6c462cb0e92340",
    date: "11/07/2017",
    type: "expense",
  };
  await addRecord(formInfo);
}

// Gasto financiacion: [3000€ gasto puntual]
//     - Categoría: Vehículo
//     - Concepto: Financiación

async function ExpPunCarGen() {
  console.log("Añadiendo ingreso puntual de vehiculo");
  formInfo = {
    note: "Coche de segunda mano",
    concept_id: "60ca45145d6c462cb0e9234a",
    amount: "-2350",
    account_id: "60ca58155d6c462cb0e92364",
    date: "06/17/2017",
    type: "expense",
    vehicle_id: "60ca43865d6c462cb0e9233f",
  };
  await addRecord(formInfo);
}

async function periodicInputs(date) {
  if (!date) {
    date = moment().format("MM/DD/YYYY");
  }
  console.log("Insertamos registros periodicos");
  await IncPerStock(date);
  ExpPerGen(date);
  IncPerPaysheet(date);
  IncPerExtrahours(date);
  ExpPerWater(date)
  ExpPerLight(date)
  ExpPerHeating(date)
  ExpPerCarMtto(date)
  await ExpPerCarGas(date);
}

// Ingreso acciones: [(0.5%-0.7%) del importe actual cada mes]
//     - Categoría: Acciones
//     - Concepto: Fondos indexados
async function IncPerStock(date) {
  if (moment(date).date() != 1) {return}
  console.log("Añadiendo ingreso periodico de acciones el día 1");
  var min = 0.1 * 100;
  var max = 0.9 * 100;
  var percentaje = Math.floor(Math.random() * (max - min + 1) + min) / 10000;
  var concept_id = "60ca46525d6c462cb0e92355";
  if (!fillPrevious) {
    sumStock = await findSumStock(concept_id);
  }
  interests = 4000 * percentaje
  interests = Math.floor(interests * 100) / 100
  sumStock += interests
  formInfo = {
    concept_id: concept_id,
    note: `Beneficio acumulado del ${Math.floor(percentaje * 100 * 100) / 100}%`,
    amount: interests,
    account_id: "60ca58495d6c462cb0e92365",
    date: date,
    type: "income",
  };
  await addRecord(formInfo);
}

// Gasto compras: [(50€-300€) una vez por semana siendo aleatorio si esa semana hay o no]
//     - Categoría: Compras
//     - Concepto: Varios

async function ExpPerGen(date) {
  var precision = 100; //100 es la precision para 2 decimales
  var min = -300 * precision;
  var max = -100 * precision;
  var amount = Math.floor(Math.random() * (max - min + 1) + min) / precision;
  formInfo = {
    concept_id: "60ca45825d6c462cb0e9234d",
    note: "Compra semanal",
    amount: amount,
    account_id: "60ca58155d6c462cb0e92364",
    date: date,
    type: "expense",
  };

  if (moment(date).day() == 6 && Math.random() < 0.6) {
    //Si es el día 6 de la semana, le damos un 60% de probablidiades de que se producza el gasto
    console.log("Añadiendo gasto periodico general, esta semana toca");
    await addRecord(formInfo);
  }
}

// Ingreso nomina: [(1500€-2000€) todos los meses]
//     - Categoría: Trabajo
//     - Concepto: Nomina

async function IncPerPaysheet(date) {
  var precision = 100; //100 es la precision para 2 decimales
  var min = 900 * precision;
  var max = 1000 * precision;
  var amount = Math.floor(Math.random() * (max - min + 1) + min) / precision;
  formInfo = {
    concept_id: "60ca46145d6c462cb0e92352",
    note: "Dinerito fresco",
    amount: amount,
    account_id: "60ca58155d6c462cb0e92364",
    date: date,
    type: "income",
  };

  if (moment(date).date() == 28) {
    console.log("Añadiendo ingreso periodico de la nomina");
    await addRecord(formInfo);
  }
}

// Ingreso horas extra: [(15€-100€) mensual aleatorio si hay o no]
//     - Categoría: Trabajo
//     - Concepto: Horas extra

async function IncPerExtrahours(date) {
  
  var precision = 100; //100 es la precision para 2 decimales
  var min = 15 * precision;
  var max = 50 * precision;
  var amount = Math.floor(Math.random() * (max - min + 1) + min) / precision;
  formInfo = {
    concept_id: "60ca46325d6c462cb0e92353",
    note: "Extra money",
    amount: amount,
    account_id: "60ca58155d6c462cb0e92364",
    date: date,
    type: "income",
  };

  if (moment(date).date() == 28 && Math.random() < 0.6) {
    console.log("Añadiendo ingreso periodico de horas extra, este mes toca");
    await addRecord(formInfo);
  }
}
// Gasto combustible: [(20€-50€) mensual aleatorio si hay o no (Precio * 20-25 + kilometros actuales)]
//     - Categoría: Vehículo
//     - Concepto: Combustible

async function ExpPerCarGas(date) {
  if (!(moment(date).date() == 18 && Math.random() < 0.6)) {
    return;
  }
  var precision = 100; //100 es la precision para 2 decimales
  var min = -50 * precision;
  var max = -30 * precision;
  var amount = Math.floor(Math.random() * (max - min + 1) + min) / precision;

  var minWaste = 20;
  var maxWaste = 50;
  var amountWaste = Math.floor(
    Math.random() * (maxWaste - minWaste + 1) + minWaste
  );
  var concept_id = "60ca43e55d6c462cb0e92345";
  var milometer = prevMilometer + amountWaste * 18;
  if (!fillPrevious) {
    prevMilometer = await findVehicleLastRecord(concept_id);
    milometer = prevMilometer + amountWaste * 18;
  }

  formInfo = {
    concept_id: concept_id,
    note: "A dar de comer al transporte",
    amount: amount,
    account_id: "60ca58155d6c462cb0e92364",
    date: date,
    type: "expense",
    vehicle_id: "60ca43865d6c462cb0e9233f",
    consumption: amountWaste,
    consumption_ud: "L",
    milometer: milometer,
  };
  prevMilometer = milometer;
  console.log("Añadiendo gasto periodico del combustible del vehiculo, este mes toca");
  await addRecord(formInfo);
}

// Gasto mantenimiento: [(350€-400€) anual]
//     - Categoría: Vehículo
//     - Concepto: Mantenimiento
//     - Nota: Seguro

async function ExpPerCarMtto(date) {
  var precision = 100; //100 es la precision para 2 decimales
  var min = -430 * precision;
  var max = -400 * precision;
  var amount = Math.floor(Math.random() * (max - min + 1) + min) / precision;
  formInfo = {
    concept_id: "60ca440b5d6c462cb0e92346",
    note: "Seguro del carro",
    amount: amount,
    account_id: "60ca58155d6c462cb0e92364",
    date: date,
    type: "expense",
    vehicle_id: "60ca43865d6c462cb0e9233f",
  };
  if (moment(date).date() == 11 && moment(date).month() == 11) {
    console.log("Añadiendo gasto periodico de mantenimiento del coche, este mes toca revision");
    await addRecord(formInfo);
  }
}
// Gasto agua: [(20€-40€) bimensual, (1m3-3m3)]
//     - Categoría: Vivienda
//     - Concepto: Agua

async function ExpPerWater(date) {
  var precision = 100; //100 es la precision para 2 decimales
  var min = -40 * precision;
  var max = -20 * precision;
  var amount = Math.floor(Math.random() * (max - min + 1) + min) / precision;

  var minWaste = 1;
  var maxWaste = 3;
  var amountWaste = Math.floor(
    Math.random() * (maxWaste - minWaste + 1) + minWaste
  );
  formInfo = {
    concept_id: "60ca45925d6c462cb0e9234e",
    note: "A bañarse todos",
    amount: amount,
    account_id: "60ca58155d6c462cb0e92364",
    house_id: "60ca439e5d6c462cb0e92340",
    date: date,
    consumption: amountWaste,
    consumption_ud: "m³",
    type: "expense",
  };
  if (moment(date).date() == 2 && moment(date).month() % 2 == 0) {
    console.log("Añadiendo gasto periodico de agua, este mes toca");
    await addRecord(formInfo);
  }
}

// Gasto luz: [(20€-40€) mensual, (200kWh-300kWh)]
//     - Categoría: Vivienda
//     - Concepto: luz

async function ExpPerLight(date) {
  
  var precision = 100; //100 es la precision para 2 decimales
  var min = -100 * precision;
  var max = -70 * precision;
  var amount = Math.floor(Math.random() * (max - min + 1) + min) / precision;

  var minWaste = 200;
  var maxWaste = 300;
  var amountWaste = Math.floor(
    Math.random() * (maxWaste - minWaste + 1) + minWaste
  );
  formInfo = {
    concept_id: "60ca45a05d6c462cb0e9234f",
    note: "Apagar todo que la luz esta cara",
    amount: amount,
    account_id: "60ca58155d6c462cb0e92364",
    house_id: "60ca439e5d6c462cb0e92340",
    date: date,
    consumption: amountWaste,
    consumption_ud: "kWh",
    type: "expense",
  };
  if (moment(date).date() == 3) {
    console.log("Añadiendo gasto periodico de luz");
    await addRecord(formInfo);
  }
}

// Gasto calefaccion: [(30€-50€) mensual solo los meses desde septiembre a abril]
//     - Categoría: Vivienda
//     - Concepto: Calefaccion

async function ExpPerHeating(date) {
  console.log("Añadiendo gasto periodico de calefaccion");
  var precision = 100; //100 es la precision para 2 decimales
  var min = -100 * precision;
  var max = -30 * precision;
  var amount = Math.floor(Math.random() * (max - min + 1) + min) / precision;

  var minWaste = 600;
  var maxWaste = 1000;
  var amountWaste = Math.floor(
    Math.random() * (maxWaste - minWaste + 1) + minWaste
  );
  formInfo = {
    concept_id: "60ca45c45d6c462cb0e92350",
    note: "Parece que hace frio",
    amount: amount,
    account_id: "60ca58155d6c462cb0e92364",
    house_id: "60ca439e5d6c462cb0e92340",
    date: date,
    consumption: amountWaste,
    consumption_ud: "kWh",
    type: "expense",
  };
  if (
    moment(date).date() == 4 &&
    (moment(date).month() >= 10 || moment(date).month() <= 4)
  ) {
    console.log("Añadiendo gasto periodico de calefaccion, este mes es frio");

    await addRecord(formInfo);
  }
}

async function from2017ToNow() {
  date = moment("06/07/2017"); //fecha inicial
  fillPrevious = true;
  puntualInputs();
  while (date <= moment()) {
    periodicInputs(date.format("MM/DD/YYYY"));
    date = date.add(1, "days");
  }
}

async function addRecord(info) {
  console.log("Añadiendo registro");
  var {
    concept_id,
    concept_name,
    concept_group,
    concept_subgroup,
    category_id,
    category_name,
    note,
    amount,
    account_id,
    account_to_id,
    account_name,
    account_color,
    currency_id,
    currency_symbol,
    currency_format,
    has_main_currency,
    vehicle_id,
    vehicle_name,
    vehicle_power_source,
    house_id,
    house_name,
    date,
    gross_amount,
    consumption,
    consumption_ud,
    milometer,
    type,
    transfer_type,
  } = info;
  var newRecord2;

  if (account_to_id) {
    category_id = "source";
    category_name = "Origen";
    concept_id = "transfer";
    concept_name = "Transferencia";
    concept_group = transfer_type;
  } else {
    await Concept.findById(concept_id, (err, result) => {
      if (err)
        return result.status(400).json({
          message: `Error trying to get the Concept from the db: ${err}`,
        });

      if (!result)
        return result
          .status(404)
          .json({ message: `The Concept doesn't exist` });

      concept_name = result.name;
      concept_group = result.group;
      concept_subgroup = result.subgroup;
      category_id = result.category_id;
      category_name = result.category_name;
    });
  }
  await Account.findById(account_id, (err, result) => {
    if (err)
      return result.status(400).json({
        message: `Error trying to get the Account from the db: ${err}`,
      });

    if (!result)
      return result.status(404).json({ message: `The Account doesn't exist` });

    account_name = result.name;
    account_color = result.color;
    currency_id = result.currency_id;
    currency_symbol = result.symbol;
    currency_format = result.format;
    has_main_currency = result.has_main_currency;
  });

  await Vehicle.findById(vehicle_id, (err, result) => {
    if (err) return; //result.status(400).json({
    //   message: `Error trying to get the Vehicle from the db: ${err}`,
    // });

    if (!result) return; //result.status(404).json({ message: `The Vehicle doesn't exist` });

    vehicle_name = result.name;
    vehicle_power_source = result.power_source;
  });

  await House.findById(house_id, (err, result) => {
    if (err) return; //result.status(400).json({
    //   message: `Error trying to get the House from the db: ${err}`,
    // });

    if (!result) return; // result.status(404).json({ message: `The House doesn't exist` });

    house_name = result.name;
  });

  const newRecord = new Record({
    user_id,
    concept_id,
    concept_name,
    concept_group,
    concept_subgroup,
    category_id,
    category_name,
    note,
    amount,
    account_id,
    account_name,
    account_color,
    currency_id,
    currency_symbol,
    currency_format,
    has_main_currency,
    vehicle_id,
    vehicle_name,
    vehicle_power_source,
    house_id,
    house_name,
    date,
    gross_amount,
    consumption,
    consumption_ud,
    milometer,
    type,
  });

  if (account_to_id) {
    var transfer_oid = newRecord._id;
    category_id = "destination";
    category_name = "Destino";
    amount = -amount;
    account_id = account_to_id;

    await Account.findById(account_id, (err, result) => {
      if (err)
        return result.status(400).json({
          message: `Error trying to get the Account from the db: ${err}`,
        });

      if (!result)
        return result
          .status(404)
          .json({ message: `The Account doesn't exist` });

      account_name = result.name;
      account_color = result.color;
      currency_id = result.currency_id;
      currency_symbol = result.symbol;
      currency_format = result.format;
      has_main_currency = result.has_main_currency;
    });

    newRecord2 = new Record({
      user_id,
      concept_id,
      concept_name,
      concept_group,
      concept_subgroup,
      category_id,
      category_name,
      note,
      amount,
      account_id,
      account_name,
      account_color,
      currency_id,
      currency_symbol,
      currency_format,
      has_main_currency,
      vehicle_id,
      vehicle_name,
      vehicle_power_source,
      house_id,
      house_name,
      date,
      gross_amount,
      consumption,
      consumption_ud,
      milometer,
      type,
      transfer_oid,
    });
    newRecord.transfer_oid = newRecord2._id;
  }

  await newRecord.save((err, result) => {
    if (err)
      return result.status(400).json({
        message: `Error trying to save the ${COLLECTION} in the db: ${err}`,
      });
    result.user_id = undefined;
  });
  if (account_to_id) {
    await newRecord2.save((err, result) => {
      if (err)
        return result.status(400).json({
          message: `Error trying to save the ${COLLECTION} in the db: ${err}`,
        });

      result.user_id = undefined;
    });
  }
}

async function findVehicleLastRecord(concept_id) {
  var milometer = await Record.findOne({ concept_id: concept_id }).sort({
    date: -1,
  });
  if (!milometer) {
    milometer = 0;
  } else {
    milometer = milometer.milometer;
  }
  return milometer;
}

async function findSumStock(concept_id) {
  var sum = await Record.aggregate([
    { $match: { concept_id: concept_id } },
    { $group: { _id: null, amount: { $sum: "$amount" } } }
])
  if (!sum) {
    sum = 0;
  } else {
    sum = sum[0].amount + initialStock;
  }
  return sum;
}

module.exports = {
  periodicInputs,
  from2017ToNow,
};
