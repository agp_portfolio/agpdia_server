
function diacriticSensitiveRegex(string = "") {
    return string
      .replace(/a|á|à|ä/g, "[a,á,à,ä]")
      .replace(/e|é|ë/g, "[e,é,ë]")
      .replace(/i|í|ï/g, "[i,í,ï]")
      .replace(/o|ó|ö|ò/g, "[o,ó,ö,ò]")
      .replace(/u|ü|ú|ù/g, "[u,ü,ú,ù]");
  }

module.exports = diacriticSensitiveRegex;
