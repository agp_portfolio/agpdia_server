// For the creation a mongoose schema it's necesary import mongose
const mongoose = require("mongoose");
const { Schema } = mongoose;

// Schema
const TravelSchema = new Schema({
  user_id: { type: String, select: false },
  date: { type: Date, required: true },
  continent: { type: String, required: true },
  continentCode: { type: String, required: true },
  country: { type: String, required: true },
  countryCode: { type: String, required: true },
  region: { type: String },
  regionCode: { type: String },
  latitude: { type: Number },
  longitude: { type: Number },
  place: { type: String },
  annotation: { type: String },
  is_scheduled: { type: Boolean },
});

// Schema export
module.exports = mongoose.model("Travels_Travel", TravelSchema);
