// For the creation a mongoose schema it's necesary import mongose
const mongoose = require("mongoose");
const { Schema } = mongoose;

// Schema
const AccountsConceptSchema = new Schema({
  user_id: { type: String, select: false },
  category_id: { type: String, required: true },
  category_name: { type: String, required: true },
  group: {
    type: String,
    enum: ["general","house", "vehicle", "stock"],
    default: "general"
  },
  subgroup: {
    type: String,
    enum: [
      "general",
      "paysheet",
      "water_house",
      "light_house",
      "gas_house",
      "power_source_vehicle",
      "maintenance_vehicle",
    ],
    default: "general"
  },
  name: { type: String, required: true },
});

// Schema export
module.exports = mongoose.model("Accounts_concept", AccountsConceptSchema);
