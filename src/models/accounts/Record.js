// For the creation a mongoose schema it's necesary import mongose
const mongoose = require("mongoose");
const { Schema } = mongoose;

// Schema
const AccountsRecordSchema = new Schema({
  user_id: { type: String, select: false },
  concept_id: { type: String, required: true }, // If it is a transfer this will be transfer
  concept_name: { type: String, required: true }, // If it is a transfer this will be transfer in the designated language
  concept_group: { type: String },
  concept_subgroup: { type: String },
  category_id: { type: String, required: true }, // If it is a transfer this will be to/from
  category_name: { type: String, required: true }, // If it is a transfer this will be send/recive in the designated language
  type: {
    type: String,
    enum: ["income", "expense", "info", "transfer"],
    required: true,
  },
  transfer_oid: { type: String },
  note: { type: String },
  amount: { type: Number, required: true },
  account_id: { type: String, required: true },
  account_name: { type: String, required: true },
  account_color: { type: String, required: true },
  currency_id: { type: String, required: true },
  currency_symbol: { type: String, required: true },
  currency_format: { type: String, required: true },
  has_main_currency: { type: Boolean, required: true },
  vehicle_id: { type: String },
  vehicle_name: { type: String },
  vehicle_power_source: { type: String },
  house_id: { type: String },
  house_name: { type: String },
  date: { type: Date, required: true },
  gross_amount: { type: Number },
  consumption: { type: Number },
  consumption_ud: { type: String },
  milometer: { type: Number },
});

// Schema export
module.exports = mongoose.model("Accounts_record", AccountsRecordSchema);
