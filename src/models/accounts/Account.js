// For the creation a mongoose schema it's necesary import mongose
const mongoose = require("mongoose");
const { Schema } = mongoose;

// Schema
const AccountsAccountSchema = new Schema({
  user_id: { type: String, select: false },
  currency_id: { type: String, required: true },
  name: { type: String, required: true },
  initial_amount:{type:Number, default: 0, required:true},
  code: { type: String, required: true },
  symbol: { type: String, required: true },
  format: { type: String, required: true },
  image: { type: String, required: true },
  color: { type: String, required: true },
  statistics: {
    type: String,
    enum: [
      "general",
      "stock",
    ],
    default: "general"
  },
  has_main_currency: { type: Boolean, required:true }, // It means if there has the main currency
});

// Schema export
module.exports = mongoose.model("Accounts_account", AccountsAccountSchema);
