// For the creation a mongoose schema it's necesary import mongose
const mongoose = require("mongoose");
const { Schema } = mongoose;

// Schema
const LocationSchema = new Schema({
  user_id: { type: String, select: false },
  name: { type: String, required: true },
  inventory_id: { type: String, required: true },
  inventory_name: { type: String, required: true },
});

// Schema export
module.exports = mongoose.model("Inventories_Location", LocationSchema);
