// For the creation a mongoose schema it's necesary import mongose
const mongoose = require("mongoose");
const { Schema } = mongoose;

// Schema
const CountrySchema = new Schema({
  name: { type: String },
  code: { type: String },
  continent: { type: String },
});

// Schema export
module.exports = mongoose.model("Country", CountrySchema);
