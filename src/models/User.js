// For the creation a mongoose schema it's necesary import mongose
const mongoose = require("mongoose");
const { Schema } = mongoose;
const bcrypt = require("bcrypt-nodejs");
const crypto = require("crypto");

// Schema
const UserSchema = new Schema({
  email: {
    type: String,
    unique: true,
    lowercase: true,
  },
  display_name: {
    type: String,
  },
  avatar: { type: String , default: 'generic.png'},
  password: {
    type: String,
  },
  singnup_date: { type: Date, default: Date.now() },
  last_login: { type: Date },
  // is_admin: {
  //   type: Boolean,
  //   required: true,
  //   default: false,
  // },
  language: {
    type: String,
    required: true,
    default: "ES",
  },
});

UserSchema.pre("save", function (next) {
  let user = this;

  bcrypt.genSalt(10, (err, salt) => {
    if (err) return next();

    bcrypt.hash(user.password, salt, null, (err, hash) => {
      if (err) return next(err);

      user.password = hash;
      next();
    });
  });
});

UserSchema.methods.comparePassword = function(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
      if (err) return cb(err);
      cb(null, isMatch);
  });
};

// Schema export
module.exports = mongoose.model("User", UserSchema);
