// For the creation a mongoose schema it's necesary import mongose
const mongoose = require("mongoose");
const { Schema } = mongoose;

// Schema
const CurrencySchema = new Schema({
  user_id: { type: String, select: false },
  code: { type: String, required: true },
  symbol: { type: String, required: true },
  format: {
    type: String,
    enum: ["¤ #.###,##", "¤ #,###.##", "#.###,## ¤", "#,###.## ¤"],
    required: true,
  },
  is_main: { type: Boolean, default: false },
  exchange: { type: Number, default: 1 }, // Exchange value 1 unity of this currency
});

// Schema export
module.exports = mongoose.model("Currency", CurrencySchema);
