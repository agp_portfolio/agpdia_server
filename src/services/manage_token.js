const jwt = require("jwt-simple");
const moment = require("moment");
const config = require("../config/config");
const manageId = require("./manage_id");

function createToken(user) {
  const codifiedUserId = manageId.codify(user._id);
  
  const payload = {
    sub: codifiedUserId,
    iat: moment().unix(), // Date of creation in unix format
    exp: moment().add(14, "day").unix(), // Date of expiration in unix format
    lng: user.language,
  };

  return jwt.encode(payload, config.SECRET_TOKEN);
}

function decodeToken(token) {
  const decoded = new Promise((resolve, reject) => {
    try {
      const payload = jwt.decode(token, config.SECRET_TOKEN);
      if (payload.exp <= moment().unix())
        reject({
          status: 401,
          message: "The Token has expired",
        });

      const userId = manageId.decodify(payload.sub);
      const userLanguage = payload.lng;

      resolve({userId,userLanguage});
    } catch (err) {
      reject({
        status: 400,
        message: "Invalid Token",
      });
    }
  });

  return decoded;
}

module.exports = { createToken, decodeToken };
