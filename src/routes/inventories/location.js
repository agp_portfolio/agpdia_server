const express = require("express");
const router = express.Router();

const Location = require("../../models/inventories/Location");
const Item = require("../../models/inventories/Item");
const Inventory = require("../../models/inventories/Inventory");

const COLLECTION = "Location(s)";

// Get the list of documents of collection
router.get("/", function (req, res) {
  const user_id = req.userId;

  Location.find({ user_id }, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res
        .status(404)
        .json({ message: `The ${COLLECTION} doesn't exist` });

    return res.status(200).json(result);
  });
});

// Get a document
router.get("/:id", function (req, res) {
  const { id } = req.params;

  Location.findById(id, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res
        .status(404)
        .json({ message: `The ${COLLECTION} doesn't exist` });

    return res.status(200).json(result);
  });
});

// Add a document
router.post("/", function (req, res) {
  const user_id = req.userId;
  const { inventory_id, name } = req.body;

  Inventory.findById(inventory_id, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the Inventory from the db: ${err}`,
      });

    if (!result)
      return res.status(404).json({ message: `The Inventory doesn't exist` });

    const inventory_name = result.name;

    const newLocation = new Location({
      user_id,
      name,
      inventory_id,
      inventory_name,
    });

    newLocation.save((err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to save the ${COLLECTION} in the db: ${err}`,
        });

      result.user_id = undefined;
      return res.status(200).json(result);
    });
  });
});

// Update a document
router.put("/:id", function (req, res) {
  const { id } = req.params;
  var updateData = req.body;
  const { inventory_id } = req.body;

  Inventory.findById(inventory_id, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the Inventory from the db: ${err}`,
      });

    if (!result)
      return res.status(404).json({ message: `The Inventory doesn't exist` });

    updateData.inventory_name = result.name;

    Location.findByIdAndUpdate(
      id,
      updateData,
      { runValidators: true },
      (err, result) => {
        if (err)
          return res.status(400).json({
            message: `Error trying to update the ${COLLECTION} in the db: ${err}`,
          });

        if (!result)
          return res
            .status(404)
            .json({ message: `The ${COLLECTION} doesn't exist` });

        Item.updateMany(
          { location_id: id },
          { location_name: updateData.name },
          (err, result) => {
            if (err)
              return res.status(400).json({
                message: `Error updating Items: ${err}`,
              });
            // return res.status(400).json({
            //     message: `The item has been updated successfully`,
            //   });
          }
        );
        return res.status(200).json(result);
      }
    );
  });
});

// Delete a document
router.delete("/:id", function (req, res) {
  const { id } = req.params;

  Item.exists({ location_id: id }, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying checking if it's: ${err}`,
      });
    if (result)
      return res.status(400).json({
        message: `The location it's used`,
      });
    Location.findByIdAndDelete(id, (err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to delete the ${COLLECTION} from the db: ${err}`,
        });

      if (!result)
        return res.status(400).json({
          message: `The ${COLLECTION} doesn't exist`,
        });

      return res
        .status(200)
        .json({ message: `The ${COLLECTION} has been deleted` });
    });
  });
});

module.exports = router;
