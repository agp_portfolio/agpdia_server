const express = require("express");
const router = express.Router();

const Item = require("../../models/inventories/Item");
const Location = require("../../models/inventories/Location");
const Group = require("../../models/inventories/Group");
const Inventory = require("../../models/inventories/Inventory");
const Currency = require("../../models/Currency");

const COLLECTION = "Item(s)";

// Get the list of documents of collection
router.get("/", function (req, res) {
  const user_id = req.userId;

  Item.find({ user_id }, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res
        .status(404)
        .json({ message: `The ${COLLECTION} doesn't exist` });

    return res.status(200).json(result);
  });
});

// Get a document
router.get("/:id", function (req, res) {
  const { id } = req.params;

  Item.findById(id, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res
        .status(404)
        .json({ message: `The ${COLLECTION} doesn't exist` });

    return res.status(200).json(result);
  });
});

// Add a document
router.post("/", function (req, res) {
  const user_id = req.userId;
  const {
    name,
    currency_id,
    inventory_id,
    location_id,
    group_id,
    brand,
    identifier,
    acquisition_place,
    acquisition_date,
    acquisition_price,
    dispose_date,
    note,
    state,
    image,
    invoice,
  } = req.body;
  Currency.findById(currency_id, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the Currency from the db: ${err}`,
      });

    if (!result)
      return res.status(404).json({ message: `The Currency doesn't exist` });

    const currency_symbol = result.symbol;
    const currency_format = result.format;
    const has_main_currency = result.is_main;
    Inventory.findById(inventory_id, (err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to get the INventory from the db: ${err}`,
        });

      if (!result)
        return res.status(404).json({ message: `The Inventory doesn't exist` });

      const inventory_name = result.name;

      Location.findById(location_id, (err, result) => {
        if (err)
          return res.status(400).json({
            message: `Error trying to get the Location from the db: ${err}`,
          });

        if (!result)
          return res
            .status(404)
            .json({ message: `The Location doesn't exist` });

        const location_name = result.name;

        Group.findById(group_id, (err, result) => {
          if (err)
            return res.status(400).json({
              message: `Error trying to get the Group from the db: ${err}`,
            });

          if (!result)
            return res.status(404).json({ message: `The Group doesn't exist` });

          const group_name = result.name;

          const newItem = new Item({
            user_id,
            name,
            currency_id,
            currency_symbol,
            currency_format,
            has_main_currency,
            inventory_id,
            inventory_name,
            location_id,
            location_name,
            group_id,
            group_name,
            brand,
            identifier,
            acquisition_place,
            acquisition_date,
            acquisition_price,
            dispose_date,
            note,
            state,
            image,
            invoice,
          });

          newItem.save((err, result) => {
            if (err)
              return res.status(400).json({
                message: `Error trying to save the ${COLLECTION} in the db: ${err}`,
              });

            result.user_id = undefined;

            return res.status(200).json(result);
          });
        });
      });
    });
  });
});

// Update a document
router.put("/:id", function (req, res) {
  const { id } = req.params;
  const updateData = req.body;
  const { currency_id, location_id, group_id, inventory_id } = req.body;

  updateData.$unset = {};
  if (updateData.brand == undefined) updateData.$unset.brand = 1;
  if (updateData.acquisition_place == undefined)
    updateData.$unset.acquisition_place = 1;
  if (updateData.identifier == undefined) updateData.$unset.identifier = 1;
  if (updateData.note == undefined) updateData.$unset.note = 1;
  if (updateData.dispose_date == undefined) updateData.$unset.dispose_date = 1;
  if (updateData.image == undefined) updateData.$unset.image = 1;
  if (updateData.invoice == undefined) updateData.$unset.invoice = 1;

  Currency.findById(currency_id, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the Currency from the db: ${err}`,
      });

    if (!result)
      return res.status(404).json({ message: `The Currency doesn't exist` });

    updateData.currency_symbol = result.symbol;
    updateData.currency_format = result.format;
    updateData.has_main_currency = result.is_main;

    Inventory.findById(inventory_id, (err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to get the Inventory from the db: ${err}`,
        });

      if (!result)
        return res.status(404).json({ message: `The Inventory doesn't exist` });

      updateData.inventory_name = result.name;
      Location.findById(location_id, (err, result) => {
        if (err)
          return res.status(400).json({
            message: `Error trying to get the Location from the db: ${err}`,
          });

        if (!result)
          return res
            .status(404)
            .json({ message: `The Location doesn't exist` });

        updateData.location_name = result.name;

        Group.findById(group_id, (err, result) => {
          if (err)
            return res.status(400).json({
              message: `Error trying to get the Group from the db: ${err}`,
            });

          if (!result)
            return res.status(404).json({ message: `The Group doesn't exist` });

          updateData.group_name = result.name;

          Item.findByIdAndUpdate(
            id,
            updateData,
            { runValidators: true },
            (err, result) => {
              if (err)
                return res.status(400).json({
                  message: `Error trying to update the ${COLLECTION} in the db: ${err}`,
                });

              if (!result)
                return res
                  .status(404)
                  .json({ message: `The ${COLLECTION} doesn't exist` });

              return res.status(200).json(result);
            }
          );
        });
      });
    });
  });
});

// Delete a document
router.delete("/:id", function (req, res) {
  const { id } = req.params;

  Item.findByIdAndDelete(id, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to delete the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res.status(400).json({
        message: `The ${COLLECTION} doesn't exist`,
      });

    return res
      .status(200)
      .json({ message: `The ${COLLECTION} has been deleted` });
  });
});

module.exports = router;
