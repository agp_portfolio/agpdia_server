const express = require("express");
const router = express.Router();

const Group = require("../../models/inventories/Group");
const Item = require("../../models/inventories/Item");

const COLLECTION = "Group(s)";

// Get the list of documents of collection
router.get("/", function (req, res) {
  const user_id = req.userId;

  Group.find({ user_id }, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res
        .status(404)
        .json({ message: `The ${COLLECTION} doesn't exist` });

    return res.status(200).json( result );
  });
});

// Get a document
router.get("/:id", function (req, res) {
  const { id } = req.params;

  Group.findById(id, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res
        .status(404)
        .json({ message: `The ${COLLECTION} doesn't exist` });

    return res.status(200).json( result );
  });
});

// Add a document
router.post("/", function (req, res) {
  const user_id = req.userId;
  const { name } = req.body;

  const newGroup = new Group({ user_id, name });

  newGroup.save((err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to save the ${COLLECTION} in the db: ${err}`,
      });

    result.user_id = undefined;
    return res.status(200).json( result );
  });
});

// Update a document
router.put("/:id", function (req, res) {
  const { id } = req.params;
  const updateData = req.body;

  Group.findByIdAndUpdate(
    id,
    updateData,
    { runValidators: true },
    (err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to update the ${COLLECTION} in the db: ${err}`,
        });

      if (!result)
        return res
          .status(404)
          .json({ message: `The ${COLLECTION} doesn't exist` });

      Item.updateMany(
        { group_id: id },
        { group_name: updateData.name },
        (err, result) => {
          if (err)
            return res.status(400).json({
              message: `Error updating Items: ${err}`,
            });
          // return res.status(400).json({
          //     message: `The item has been updated successfully`,
          //   });
        }
      );
      return res.status(200).json(result);
    }
  );
});

// Delete a document
router.delete("/:id", function (req, res) {
  const { id } = req.params;

  Item.exists({ group_id: id }, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying checking if it's: ${err}`,
      });
    if (result)
      return res.status(400).json({
        message: `The group it's used`,
      });
    Group.findByIdAndDelete(id, (err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to delete the ${COLLECTION} from the db: ${err}`,
        });

      if (!result)
        return res.status(400).json({
          message: `The ${COLLECTION} doesn't exist`,
        });

      return res
        .status(200)
        .json({ message: `The ${COLLECTION} has been deleted` });
    });
  });
});

module.exports = router;
