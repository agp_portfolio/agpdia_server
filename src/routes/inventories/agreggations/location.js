const express = require("express");
const router = express.Router();

const Location = require("../../../models/inventories/Location");

const COLLECTION = "Location(s)";

// Get the list of documents of collection
router.get("/filter_list", (req, res) => {
  const user_id = req.userId;
  filter = {
    user_id: user_id,
  };
  Location.aggregate(
    [
      {
        $match: filter,
      },
      { $sort: { name: 1 } },
      {
        $group: {
          _id: "$inventory_id",
          locations: {
            $push: {
              id: "$_id",
              name: "$name",
            },
          },
          name: { $first: "$inventory_name" },
        },
      },
      { $sort: { name: 1 } },
    ],
    (err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
        });

      if (!result)
        return res
          .status(404)
          .json({ message: `The ${COLLECTION} doesn't exist` });

      return res.status(200).json(result);
    }
  );
});

// Get the list of documents of collection of some inventory
router.post("/ofInventory", (req, res) => {
  const user_id = req.userId;
  const { inventory_id } = req.body;
  filter = {
    user_id: user_id,
    inventory_id: inventory_id,
  };
  Location.find(filter, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res
        .status(404)
        .json({ message: `The ${COLLECTION} doesn't exist` });

    return res.status(200).json( result );
  });
  // Location.aggregate(
  //   [
  //     {
  //       $match: filter,
  //     },
  //     { $sort: { name: 1 } },
  //     {
  //       $group: {
  //         _id: "$inventory_id",
  //         locations: {
  //           $push: {
  //             id: "$_id",
  //             name: "$name",
  //           },
  //         },
  //         name: { $first: "$inventory_name" },
  //       },
  //     },
  //     { $sort: { name: 1 } },
  //   ],
  //   (err, result) => {
  //     if (err)
  //       return res.status(400).json({
  //         message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
  //       });

  //     if (!result)
  //       return res
  //         .status(404)
  //         .json({ message: `The ${COLLECTION} doesn't exist` });

  //     return res.status(200).json(result);
  //   }
  // );
});

module.exports = router;
