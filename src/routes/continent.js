const express = require("express");
const router = express.Router();

const Continent = require("../models/Continent");

const COLLECTION = "Continent(s)";

// Get the list of documents of collection
router.get("/", function (req, res) {
  Continent.find((err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res
        .status(404)
        .json({ message: `The ${COLLECTION} doesn't exist` });
    
    return res.status(200).json( result );
  });
});

module.exports = router;
