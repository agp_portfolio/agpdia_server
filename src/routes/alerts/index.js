const express = require("express");
const router = express.Router();

router.use("/accounts", require("./account"));
router.use("/messages", require("./message"));

module.exports = router;
