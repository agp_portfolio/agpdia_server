const express = require("express");
const router = express.Router();

const Concept = require("../../models/accounts/Concept");
const Category = require("../../models/accounts/Category");
const Record = require("../../models/accounts/Record");

const COLLECTION = "Concept(s)";

// Get the list of documents of collection
router.get("/", function (req, res) {
  const user_id = req.userId;

  Concept.find({ user_id }, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res
        .status(404)
        .json({ message: `The ${COLLECTION} doesn't exist` });

    return res.status(200).json( result );
  });
});

// Get a document
router.get("/:id", function (req, res) {
  const { id } = req.params;

  Concept.findById(id, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res
        .status(404)
        .json({ message: `The ${COLLECTION} doesn't exist` });

    return res.status(200).json( result );
  });
});

// Add a document
router.post("/", function (req, res) {
  const user_id = req.userId;
  const { category_id, group, subgroup, name } = req.body;

  Category.findById(category_id, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the Category from the db: ${err}`,
      });

    if (!result)
      return res.status(404).json({ message: `The Category doesn't exist` });

    const category_name = result.name;

    const newConcept = new Concept({
      user_id,
      category_id,
      category_name,
      group,
      subgroup,
      name,
    });

    newConcept.save((err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to save the ${COLLECTION} in the db: ${err}`,
        });

      result.user_id = undefined;
      return res.status(200).json( result );
    });
  });
});

// Update a document
router.put("/:id", function (req, res) {
  const { id } = req.params;
  const updateData = req.body;
  const { category_id } = req.body;
  updateData.$unset = {};
  if (!updateData.group) updateData.$unset.group = 1;
  if (!updateData.subgroup) updateData.$unset.subgroup = 1;

  Category.findById(category_id, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the Category from the db: ${err}`,
      });

    if (!result)
      return res.status(404).json({ message: `The Category doesn't exist` });

    updateData.category_name = result.name;
    
    Concept.findByIdAndUpdate(
      id,
      updateData,
      { runValidators: true },
      (err, result) => {
        if (err)
          return res.status(400).json({
            message: `Error trying to update the ${COLLECTION} in the db: ${err}`,
          });

        if (!result)
          return res
            .status(404)
            .json({ message: `The ${COLLECTION} doesn't exist` });

        Category.updateMany(
          { concept_id: id },
          { concept_name: updateData.name },
          (err, result) => {
            if (err)
              return res.status(400).json({
                message: `Error updating Category: ${err}`,
              });
            // return res.status(400).json({
            //     message: `The Category has been updated successfully`,
            //   });
            Record.updateMany(
              { concept_id: id },
              { concept_name: updateData.name,
              category_id: category_id,
              category_name: updateData.category_name },
              (err, result) => {
                if (err)
                  return res.status(400).json({
                    message: `Error updating Records: ${err}`,
                  });
                // return res.status(400).json({
                //     message: `The Record has been updated successfully`,
                //   });
              }
            );
          }
        );
        return res.status(200).json(result);
      }
    );
  });
});

// Delete a document
router.delete("/:id", function (req, res) {
  const { id } = req.params;

  Record.exists({ concept_id: id }, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying checking if it's: ${err}`,
      });
    if (result)
      return res.status(400).json({
        message: `The Concept it's used`,
      });
    Category.exists({ concept_id: id }, (err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying checking if it's: ${err}`,
        });
      if (result)
        return res.status(400).json({
          message: `The Concept it's used`,
        });
      Concept.findByIdAndDelete(id, (err, result) => {
        if (err)
          return res.status(400).json({
            message: `Error trying to delete the ${COLLECTION} from the db: ${err}`,
          });

        if (!result)
          return res.status(400).json({
            message: `The ${COLLECTION} doesn't exist`,
          });

        return res
          .status(200)
          .json({ message: `The ${COLLECTION} has been deleted` });
      });
    });
  });
});

module.exports = router;
