const express = require("express");
const router = express.Router();

router.use("/accounts", require("./account"));
router.use("/categories", require("./category"));
router.use("/concepts", require("./concept"));
router.use("/records", require("./record"));
router.use("/records_alerts_triggers", require("./records_alerts_trigger"));
router.use("/aggs", require("./agreggations"));

module.exports = router;
