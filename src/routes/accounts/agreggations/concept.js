const express = require("express");
const router = express.Router();

const Concept = require("../../../models/accounts/Concept");
const { decodeToken } = require("../../../services/manage_token");

const COLLECTION = "Concept(s)";

// Get the list of documents of collection
router.post("/filter_list", (req, res) => {
  const user_id = req.userId;
  const { group, subgroup } = req.body;

  filter = {
    user_id: user_id,
  };
  if (group){
    filter.group = {};
    filter.group.$in = group;
  }
  if (subgroup){
    filter.subgroup = {};
    filter.subgroup.$in = subgroup;
  }
  Concept.aggregate(
    [
      {
        $match: filter,
      },
      { $sort: { name: 1 } },
      {
        $group: {
          _id: "$category_id",
          concepts: {
            $push: {
              id: "$_id",
              name: "$name",
            },
          },
          name: { $first: "$category_name" },
        },
      },
      { $sort: { name: 1 } },
    ],
    (err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
        });

      if (!result)
        return res
          .status(404)
          .json({ message: `The ${COLLECTION} doesn't exist` });

      return res.status(200).json(result);
    }
  );
});

// Get the list of documents of collection of some category
router.post("/ofCategory", (req, res) => {
  const user_id = req.userId;
  const { category_id } = req.body;
  filter = {
    user_id: user_id,
    category_id: category_id,
  };
  Concept.find(filter, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res
        .status(404)
        .json({ message: `The ${COLLECTION} doesn't exist` });

    return res.status(200).json( result );
  });
  // Concept.aggregate(
  //   [
  //     {
  //       $match: filter,
  //     },
  //     { $sort: { name: 1 } },
  //     {
  //       $group: {
  //         _id: "$category_id",
  //         concepts: {
  //           $push: {
  //             id: "$_id",
  //             name: "$name",
  //           },
  //         },
  //         name: { $first: "$category_name" },
  //       },
  //     },
  //     { $sort: { name: 1 } },
  //   ],
  //   (err, result) => {
  //     if (err)
  //       return res.status(400).json({
  //         message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
  //       });

  //     if (!result)
  //       return res
  //         .status(404)
  //         .json({ message: `The ${COLLECTION} doesn't exist` });

  //     return res.status(200).json(result);
  //   }
  // );
});

module.exports = router;
