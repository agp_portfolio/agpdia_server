const express = require("express");
const router = express.Router();

router.use("/records", require("./record"));
router.use("/analysis", require("./analysis"));
router.use("/accounts", require("./account"));
router.use("/concepts", require("./concept"));
router.use("/dashboard", require("./dashboard"));

module.exports = router;
