const express = require("express");
const router = express.Router();
var ObjectId = require("mongodb").ObjectID;

const Record = require("../../models/accounts/Record");
const Concept = require("../../models/accounts/Concept");
// const Category = require("../../models/accounts/Category");
const Account = require("../../models/accounts/Account");
const Vehicle = require("../../models/Vehicle");
const House = require("../../models/House");
const { updateUserMessages } = require("../../helpers/messagesManagement");

const COLLECTION = "Record(s)";

// Get the list of documents of collection
router.get("/", function (req, res) {
  const user_id = req.userId;

  Record.find({ user_id }, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res
        .status(404)
        .json({ message: `The ${COLLECTION} doesn't exist` });

    return res.status(200).json(result);
  });
});

// Get a document
router.get("/:id", async function (req, res) {
  const { id } = req.params;
  var response;
  try {
    await Record.findById(id, (err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
        });

      if (!result)
        return res
          .status(404)
          .json({ message: `The ${COLLECTION} doesn't exist` });

      // Clonamos el objeto {... result} y Object.assign({}, result) no clonan 
      // exactamente el objeto, aportan mas informacion y lo que nos interesa esta
      // dentro de .doc
      response = JSON.parse(JSON.stringify(result));
      if (result.type == "transfer") {
        response.account_to_id = result.account_id;
        response.account_to_name = result.account_name;
      }
    });
    if (response.type == "transfer") {
      await Record.findById(response.transfer_oid, (err, result) => {
        if (err)
          return res.status(400).json({
            message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
          });

        if (!result)
          return res
            .status(404)
            .json({ message: `The ${COLLECTION} doesn't exist` });
        //Source will always go first
        if (result.category_id == "source") {
          response._id = result._id;
          response.category_id = result.category_id;
          response.category_name = result.category_name;
          response.concept_id = result.concept_id;
          response.concept_group = result.concept_group;
          response.concept_subgroup = result.concept_subgroup;
          response.concept_name = result.concept_name;
          response.account_id = result.account_id;
          response.account_name = result.account_name;
          response.transfer_oid = result.transfer_oid;
        } else {
          response.account_to_id = result.account_id;
          response.account_to_name = result.account_name;
        }
      });
    }
  } catch (err) {
    console.log(err);
    return res
      .status(400)
      .json({ message: "There was an error getting the list: " + err });
  }
  // console.log(response)
  return res.status(200).json(response);
});

// Add a document
router.post("/", async function (req, res) {
  const user_id = req.userId;
  var {
    concept_id,
    concept_name,
    concept_group,
    concept_subgroup,
    category_id,
    category_name,
    note,
    amount,
    account_id,
    account_to_id,
    account_name,
    account_color,
    currency_id,
    currency_symbol,
    currency_format,
    has_main_currency,
    vehicle_id,
    vehicle_name,
    vehicle_power_source,
    house_id,
    house_name,
    date,
    gross_amount,
    consumption,
    consumption_ud,
    milometer,
    type,
    transfer_type,
  } = req.body;
  var newRecord2;

  if (account_to_id) {
    category_id = "source";
    category_name = "Origen";
    concept_id = "transfer";
    concept_name = "Transferencia";
    concept_group = transfer_type;
  } else {
    await Concept.findById(concept_id, (err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to get the Concept from the db: ${err}`,
        });

      if (!result)
        return res.status(404).json({ message: `The Concept doesn't exist` });

      concept_name = result.name;
      concept_group = result.group;
      concept_subgroup = result.subgroup;
      category_id = result.category_id;
      category_name = result.category_name;
    });
  }
  await Account.findById(account_id, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the Account from the db: ${err}`,
      });

    if (!result)
      return res.status(404).json({ message: `The Account doesn't exist` });

    account_name = result.name;
    account_color = result.color;
    currency_id = result.currency_id;
    currency_symbol = result.symbol;
    currency_format = result.format;
    has_main_currency = result.has_main_currency;
  });

  await Vehicle.findById(vehicle_id, (err, result) => {
    if (err) return; //res.status(400).json({
    //   message: `Error trying to get the Vehicle from the db: ${err}`,
    // });

    if (!result) return; //res.status(404).json({ message: `The Vehicle doesn't exist` });

    vehicle_name = result.name;
    vehicle_power_source = result.power_source;
  });

  await House.findById(house_id, (err, result) => {
    if (err) return; //res.status(400).json({
    //   message: `Error trying to get the House from the db: ${err}`,
    // });

    if (!result) return; // res.status(404).json({ message: `The House doesn't exist` });

    house_name = result.name;
  });

  const newRecord = new Record({
    user_id,
    concept_id,
    concept_name,
    concept_group,
    concept_subgroup,
    category_id,
    category_name,
    note,
    amount,
    account_id,
    account_name,
    account_color,
    currency_id,
    currency_symbol,
    currency_format,
    has_main_currency,
    vehicle_id,
    vehicle_name,
    vehicle_power_source,
    house_id,
    house_name,
    date,
    gross_amount,
    consumption,
    consumption_ud,
    milometer,
    type,
  });

  if (account_to_id) {
    var transfer_oid = newRecord._id;
    category_id = "destination";
    category_name = "Destino";
    amount = -amount;
    account_id = account_to_id;

    await Account.findById(account_id, (err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to get the Account from the db: ${err}`,
        });

      if (!result)
        return res.status(404).json({ message: `The Account doesn't exist` });

      account_name = result.name;
      account_color = result.color;
      currency_id = result.currency_id;
      currency_symbol = result.symbol;
      currency_format = result.format;
      has_main_currency = result.has_main_currency;
    });

    newRecord2 = new Record({
      user_id,
      concept_id,
      concept_name,
      concept_group,
      concept_subgroup,
      category_id,
      category_name,
      note,
      amount,
      account_id,
      account_name,
      account_color,
      currency_id,
      currency_symbol,
      currency_format,
      has_main_currency,
      vehicle_id,
      vehicle_name,
      vehicle_power_source,
      house_id,
      house_name,
      date,
      gross_amount,
      consumption,
      consumption_ud,
      milometer,
      type,
      transfer_oid,
    });
    newRecord.transfer_oid = newRecord2._id;
  }

  await newRecord.save((err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to save the ${COLLECTION} in the db: ${err}`,
      });
    updateUserMessages(user_id);
    result.user_id = undefined;
  });
  if (account_to_id) {
    await newRecord2.save((err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to save the ${COLLECTION} in the db: ${err}`,
        });

      result.user_id = undefined;
    });
  }
  return res.status(200).json({});
});

// Update a document
router.put("/:id", async function (req, res) {
  const user_id = req.userId;
  const { id } = req.params;
  const updateData = req.body;
  const { vehicle_id, house_id, date, account_to_id, type } = req.body;
  var { concept_id, account_id } = req.body;
  var response;
  updateData.$unset = {};
  if (!updateData.note && date) updateData.$unset.note = 1;
  if (!updateData.vehicle_id && date) {
    updateData.$unset.vehicle_id = 1;
    updateData.$unset.vehicle_name = 1;
    updateData.$unset.vehicle_power_source = 1;
  }
  if (!updateData.house_id && date) {
    updateData.$unset.house_id = 1;
    updateData.$unset.house_name = 1;
  }
  if (updateData.gross_amount == undefined && date)
    updateData.$unset.gross_amount = 1;
  if (updateData.consumption == undefined && date) {
    updateData.$unset.consumption = 1;
    updateData.$unset.consumption_ud = 1;
  }
  if (updateData.milometer == undefined && concept_id && account_id)
    updateData.$unset.milometer = 1;
  if (type != "transfer") {
    updateData.$unset.transfer_oid = 1;
  } else {
    // var previous = await Record.findById(id);
    // Las transrencias siempre se guardan como origen y se crea
    // despues el destino por lo que el importe inicial siempre es negativo
    var isNegative = /^\-[1-9]\d{0,2}(\.\d*)?$/i.test(updateData.amount);
    if (!isNegative) {
      updateData.amount = -updateData.amount;
    }
  }

  try {
    if (account_to_id) {
      updateData.category_id = "source";
      updateData.category_name = "Origen";
      updateData.concept_id = "transfer";
      updateData.concept_name = "Transferencia";
      updateData.concept_group = updateData.transfer_type;
    } else {
      await Concept.findById(concept_id, (err, result) => {
        if (err) return; //res.status(400).json({
        //   message: `Error trying to get the Concept from the db: ${err}`,
        // });

        if (!result) return; //res.status(404).json({ message: `The Concept doesn't exist` });

        updateData.concept_name = result.name;
        updateData.category_id = result.category_id;
        updateData.category_name = result.category_name;
        if (result.group) {
          updateData.concept_group = result.group;
        } else {
          updateData.$unset.concept_group = 1;
        }
        if (result.subgroup) {
          updateData.concept_subgroup = result.subgroup;
        } else {
          updateData.$unset.concept_subgroup = 1;
        }
      });
    }
    if (updateData.concept_group != "vehicle") {
      updateData.$unset.vehicle_id = 1;
      updateData.$unset.vehicle_name = 1;
      updateData.$unset.vehicle_power_source = 1;
    }
    if (updateData.concept_group != "house") {
      updateData.$unset.house_id = 1;
      updateData.$unset.house_name = 1;
    }
    if (updateData.concept_subgroup != "paysheet")
      updateData.$unset.gross_amount = 1;
    if (
      ![
        "water_house",
        "light_house",
        "gas_house",
        "power_source_vehicle",
      ].includes(updateData.concept_subgroup)
    ) {
      updateData.$unset.consumption = 1;
      updateData.$unset.consumption_ud = 1;
    }

    if (
      !["power_source_vehicle", "maintenance_vehicle"].includes(
        updateData.concept_subgroup
      )
    )
      updateData.$unset.milometer = 1;

    await Account.findById(account_id, (err, result) => {
      if (err) return; //res.status(400).json({
      //   message: `Error trying to get the Account from the db: ${err}`,
      // });

      if (!result) return; //res.status(404).json({ message: `The Account doesn't exist` });

      updateData.account_name = result.name;
      updateData.account_color = result.color;
      updateData.currency_id = result.currency_id;
      updateData.currency_symbol = result.symbol;
      updateData.currency_format = result.format;
      updateData.has_main_currency = result.has_main_currency;
    });

    await Vehicle.findById(vehicle_id, (err, result) => {
      if (err) return; //res.status(400).json({
      //   message: `Error trying to get the Vehicle from the db: ${err}`,
      // });

      if (!result) return; //res.status(404).json({ message: `The Vehicle doesn't exist` });

      updateData.vehicle_name = result.name;
      updateData.vehicle_power_source = result.power_source;
    });

    await House.findById(house_id, (err, result) => {
      if (err) return; //res.status(400).json({
      //   message: `Error trying to get the House from the db: ${err}`,
      // });

      if (!result) return; //res.status(404).json({ message: `The House doesn't exist` });

      updateData.house_name = result.name;
    });
    var account_to_id_before;
    console.log("updateData -> ", updateData);
    await Record.findByIdAndUpdate(
      id,
      updateData,
      { runValidators: true },
      (err, result) => {
        if (err)
          return res.status(400).json({
            message: `Error trying to update the ${COLLECTION} in the db: ${err}`,
          });

        if (!result)
          return res
            .status(404)
            .json({ message: `The ${COLLECTION} doesn't exist` });

        updateUserMessages(user_id);

        account_to_id_before = result.transfer_oid;
        response = result;
      }
    );

    switch (true) {
      case account_to_id && account_to_id_before && true:
        // Edit related
        updateData.account_id = account_to_id;
        await Account.findById(account_to_id, (err, result) => {
          if (err) return; //res.status(400).json({
          //   message: `Error trying to get the Account from the db: ${err}`,
          // });

          if (!result) return; //res.status(404).json({ message: `The Account doesn't exist` });

          updateData.account_name = result.name;
          updateData.account_color = result.color;
          updateData.currency_id = result.currency_id;
          updateData.currency_symbol = result.symbol;
          updateData.currency_format = result.format;
          updateData.has_main_currency = result.has_main_currency;
        });
        updateData.amount = -updateData.amount;
        updateData.category_id = "destination";
        updateData.category_name = "Destino";

        await Record.findByIdAndUpdate(
          account_to_id_before,
          updateData,
          { runValidators: true },
          (err, result) => {
            if (err)
              return res.status(400).json({
                message: `Error trying to update the ${COLLECTION} in the db: ${err}`,
              });

            if (!result)
              return res
                .status(404)
                .json({ message: `The ${COLLECTION} doesn't exist` });
          }
        );
        break;
      case account_to_id && !account_to_id_before && true:
        // Edit this and add related

        var transfer_oid = response._id;
        var category_id = "destination";
        var category_name = "Destino";
        var concept_id = "transfer";
        var concept_name = "Transferencia";
        var amount = -updateData.amount;
        var account_id = account_to_id;
        var account_name,
          account_color,
          currency_id,
          currency_symbol,
          currency_format,
          has_main_currency;
        // var type = updateData.type;
        var note = updateData.note;

        await Account.findById(account_id, (err, result) => {
          if (err)
            return res.status(400).json({
              message: `Error trying to get the Account from the db: ${err}`,
            });

          if (!result)
            return res
              .status(404)
              .json({ message: `The Account doesn't exist` });

          account_name = result.name;
          account_color = result.color;
          currency_id = result.currency_id;
          currency_symbol = result.symbol;
          currency_format = result.format;
          has_main_currency = result.has_main_currency;
        });

        var newRecord2 = new Record({
          user_id,
          concept_id,
          concept_name,
          // concept_group,
          // concept_subgroup,
          category_id,
          category_name,
          note,
          amount,
          account_id,
          account_name,
          account_color,
          currency_id,
          currency_symbol,
          currency_format,
          has_main_currency,
          // vehicle_id,
          // vehicle_name,
          // vehicle_power_source,
          // house_id,
          // house_name,
          date,
          // gross_amount,
          // consumption,
          // consumption_ud,
          // milometer,
          type,
          transfer_oid,
        });

        var transfer_oid_destiny = newRecord2._id;

        await Record.findByIdAndUpdate(
          id,
          { transfer_oid: transfer_oid_destiny },
          { runValidators: true },
          (err, result) => {
            if (err)
              return res.status(400).json({
                message: `Error trying to update the ${COLLECTION} in the db: ${err}`,
              });

            if (!result)
              return res
                .status(404)
                .json({ message: `The ${COLLECTION} doesn't exist` });

            account_to_id_before = result.transfer_oid;
            response = result;
          }
        );

        await newRecord2.save((err, result) => {
          if (err)
            return res.status(400).json({
              message: `Error trying to save the ${COLLECTION} in the db: ${err}`,
            });

          result.user_id = undefined;
        });

        break;
      case !account_to_id && account_to_id_before && true:
        // Delete related
        await Record.findByIdAndDelete(account_to_id_before, (err, result) => {
          if (err)
            return res.status(400).json({
              message: `Error trying to delete the ${COLLECTION} from the db: ${err}`,
            });
          if (!result)
            return res.status(400).json({
              message: `The ${COLLECTION} doesn't exist`,
            });
        });
        break;
      default:
        break;
    }
    return res.status(200).json({});
  } catch (err) {
    console.log(err);
    return res
      .status(400)
      .json({ message: "There was an error getting the list: " + err });
  }
});

// Delete a document
router.delete("/:id", async function (req, res) {
  const user_id = req.userId;
  const { id } = req.params;
  var array = id.split(",");
  var arrayObjects = [];
  array.forEach((string) => {
    arrayObjects.push(ObjectId(string));
  });

  var filter = { _id: { $in: arrayObjects } };

  await Record.deleteMany(filter, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to delete the ${COLLECTION} from the db: ${err}`,
      });
    if (!result)
      return res.status(400).json({
        message: `The ${COLLECTION} doesn't exist`,
      });
  });
  updateUserMessages(user_id);
  return res
    .status(200)
    .json({ message: `The ${COLLECTION} has been deleted` });
});

module.exports = router;
