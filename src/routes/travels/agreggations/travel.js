const express = require("express");
const router = express.Router();

const Travel = require("../../../models/travels/Travel");
const { decodeToken } = require("../../../services/manage_token");
const diacriticSensitiveRegex = require("../../../helpers/diacriticSensitiveRegex");

const COLLECTION = "Item(s)";

// Get the list of documents of collection
router.post("/list", async (req, res) => {
  const user_id = req.userId;
  const { start_date, end_date, countryCode, continentCode, regionCode, note } = req.body;
  filter = {
    user_id: user_id,
  };
  if(start_date || end_date) filter.date= { $gte: new Date(start_date), $lte: new Date(end_date) }

  if (countryCode) {
    filter.countryCode = {};
    filter.countryCode.$in = countryCode;
  }
  if (continentCode) {
    filter.continentCode = {};
    filter.continentCode.$in = continentCode;
  }

  if (regionCode){
    filter.regionCode = {};
    filter.regionCode.$in = regionCode;
  }

  if (note) {
    if (!filter.$or) filter.$or = [];
    filter.$or.push({
      continent: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
    filter.$or.push({
      country: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
    filter.$or.push({
      region: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
    filter.$or.push({
      latitude: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
    filter.$or.push({
      longitude: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
    filter.$or.push({
      place: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
    filter.$or.push({
      annotation: { $regex: diacriticSensitiveRegex(note), $options: "i" },
    });
  }

  try {
    var travelsList = await Travel.aggregate([
      {
        $match: filter,
      },
      { $sort: {date: -1, _id: -1 } },
    ]);

    if (!travelsList.length)
      return res
        .status(404)
        .json({ message: "There is no data with this filter conditions" });

    return res.status(200).json(travelsList);
  } catch (err) {
    console.log(err);
    return res
      .status(400)
      .json({ message: "There was an error getting the list: " + err });
  }
});

module.exports = router;
