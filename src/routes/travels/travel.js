const express = require("express");
const router = express.Router();

const Travel = require("../../models/travels/Travel");
const Country = require("../../models/Country");
const Region = require("../../models/Region");

const COLLECTION = "Travel(s)";

// Get the list of documents of collection
router.get("/", function (req, res) {
  const user_id = req.userId;

  Travel.find({ user_id }, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res
        .status(404)
        .json({ message: `The ${COLLECTION} doesn't exist` });

    return res.status(200).json(result);
  });
});

// Get a document
router.get("/:id", function (req, res) {
  const { id } = req.params;

  Travel.findById(id, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to get the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res
        .status(404)
        .json({ message: `The ${COLLECTION} doesn't exist` });

    return res.status(200).json(result);
  });
});

// Add a document
router.post("/", function (req, res) {
  const user_id = req.userId;
  const {
    date,
    continent,
    continentCode,
    country,
    countryCode,
    region,
    regionCode,
    latitude,
    longitude,
    place,
    annotation,
    is_scheduled,
  } = req.body;

  const newTravel = new Travel({
    user_id,
    date,
    continent,
    continentCode,
    country,
    countryCode,
    region,
    regionCode,
    latitude,
    longitude,
    place,
    annotation,
    is_scheduled,
  });

  newTravel.save((err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to save the ${COLLECTION} in the db: ${err}`,
      });

    result.user_id = undefined;
    return res.status(200).json(result);
  });
});

// Update a document
router.put("/:id", function (req, res) {
  const { id } = req.params;
  const updateData = req.body;

  updateData.$unset = {};
  if (!updateData.region) updateData.$unset.region = 1;
  if (!updateData.regionCode) updateData.$unset.regionCode = 1;

  if (!updateData.longitude) updateData.$unset.longitude = 1;
  if (!updateData.latitude) updateData.$unset.latitude = 1;
  if (!updateData.place) updateData.$unset.place = 1;
  if (!updateData.annotation) {
    delete updateData['annotation']
    updateData.$unset.annotation = 1;
  }
  console.log(updateData)
  Travel.findByIdAndUpdate(
    id,
    updateData,
    { runValidators: true },
    (err, result) => {
      if (err)
        return res.status(400).json({
          message: `Error trying to update the ${COLLECTION} in the db: ${err}`,
        });

      if (!result)
        return res
          .status(404)
          .json({ message: `The ${COLLECTION} doesn't exist` });

      return res.status(200).json(result);
    }
  );
});

// Delete a document
router.delete("/:id", function (req, res) {
  const { id } = req.params;

  Travel.findByIdAndDelete(id, (err, result) => {
    if (err)
      return res.status(400).json({
        message: `Error trying to delete the ${COLLECTION} from the db: ${err}`,
      });

    if (!result)
      return res.status(400).json({
        message: `The ${COLLECTION} doesn't exist`,
      });

    return res
      .status(200)
      .json({ message: `The ${COLLECTION} has been deleted` });
  });
});

module.exports = router;
