const express = require("express");
const router = express.Router();
const isAuth = require("../middlewares/auth");

// Global routes
router.use("/user", require("./user"));
router.use("/user_infos", require("./user_info"));
router.use("/houses", isAuth, require("./house"));
router.use("/vehicles", isAuth, require("./vehicle"));
router.use("/currencies", isAuth, require("./currency"));
router.use("/currency_infos", require("./currency_info"));
router.use("/countries", require("./country"));
router.use("/regions", require("./region"));
router.use("/continents", require("./continent"));

// Alerts routes
router.use("/alerts", isAuth, require("./alerts"));

// Accounts routes
router.use("/accounts", isAuth, require("./accounts"));

// Inventories routes
router.use("/inventories", isAuth, require("./inventories"));

// Travels routes
router.use("/travels", isAuth, require("./travels"));

module.exports = router;
